package service;

import dao.CommentDao;
import model.Comment;
import model.Post;


public class CommentService {

    public static boolean addComment(Comment comment, Post post){
        return CommentDao.addComment(comment, post);
    }

    public static Post postById(int postId){
        return CommentDao.postById(postId);
    }
}
