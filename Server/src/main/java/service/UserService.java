package service;

import dao.UserDao;
import model.Post;
import model.User;

import java.util.List;

public class UserService {

    public static User login(String username, String password ){
        return UserDao.checkLogin(username, password);
    }
}
