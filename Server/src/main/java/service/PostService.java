package service;

import dao.PostDao;
import model.Post;

import java.util.Base64;
import java.util.List;

public class PostService {

    public List<Post> allPosts(){
        List<Post> posts =  PostDao.getPosts();
        for(Post post : posts) {
            post.setBase64(Base64.getEncoder().encodeToString(post.getFileimage()));
        }
        return posts;
    }

    public Post getPostById(int postId){
        return PostDao.getPostById(postId);
    }


    public boolean addPost(Post post){
        return PostDao.addPost(post);
    }

}
