import model.Comment;
import model.Post;
import model.User;
import service.UserService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

public class DbInitializer {

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("instagram");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        //Users
        User u1 = new User("yang","password");
        User u2 = new User("nour","password");

        //Comments
        Comment c1 = new Comment("comment 1");
        c1.setUser(u2);

        Comment c2 = new Comment("comment 2");
        c2.setUser(u2);

        Comment c3 = new Comment("comment 3");
        c3.setUser(u1);



        //POST1
        Post p1 = new Post();
        p1.setDescription("post 1 description");
        //save image into database
        File file = new File("Server/src/main/Resources/image3.JPG");
        byte[] bFile = new byte[(int) file.length()];
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            //convert file into array of bytes
            fileInputStream.read(bFile);
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        p1.setFileimage(bFile);
        p1.setUser(u1);
        p1.addComment(c1);


        //Post2
        Post p2 = new Post();
        p2.setDescription("post 2 description");
        File file2 = new File("Server/src/main/Resources/image1.jpeg");
        byte[] bFile2 = new byte[(int) file.length()];
        try {
            FileInputStream fileInputStream = new FileInputStream(file2);
            //convert file into array of bytes
            fileInputStream.read(bFile2);
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        p2.setFileimage(bFile2);

        p2.setUser(u1);
        p2.addComment(c2);


        //Post3
        Post p3 = new Post();
        p3.setDescription("post 3 description");
        File file3 = new File("Server/src/main/Resources/image2.jpeg");
        byte[] bFile3 = new byte[(int) file.length()];
        try {
            FileInputStream fileInputStream = new FileInputStream(file3);
            //convert file into array of bytes
            fileInputStream.read(bFile3);
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        p3.setFileimage(bFile3);

        p3.setUser(u2);
        p3.addComment(c3);


        entityManager.getTransaction().begin();
        entityManager.persist(u1);
        entityManager.persist(u2);
        entityManager.persist(c1);
        entityManager.persist(c2);
        entityManager.persist(c3);
        entityManager.persist(p1);
        entityManager.persist(p2);
        entityManager.persist(p3);
        entityManager.getTransaction().commit();

//        User user = UserService.login("nour", "password");
//        System.out.println(user);

    }
}
