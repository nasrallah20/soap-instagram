package dao;

import model.Comment;
import model.Post;

import javax.persistence.TypedQuery;


public class CommentDao extends DAO{


	public static boolean addComment(Comment comment, Post post){
		try{
			post.addComment(comment);
			entityManager.getTransaction().begin();
			entityManager.merge(post);
			entityManager.getTransaction().commit();
			return true;
		}catch (Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public static Post postById(int post_id) {
		try{
			TypedQuery<Post> query = entityManager.createQuery("select post from Post post WHERE post.idPost=:post_id", Post.class);
			return query.setParameter("post_id", post_id).getResultList().stream().findFirst().get();
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
