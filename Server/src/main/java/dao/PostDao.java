package dao;

import model.Comment;
import model.Post;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class PostDao extends DAO {

    public static List<Post> getPosts() {
        List<Post> posts = null;
        try {

            TypedQuery<Post> query = entityManager.createQuery("SELECT post FROM Post post", Post.class);
            posts = query.getResultList();

            return posts;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean addPost(Post post)
    {
        try{
            entityManager.getTransaction().begin();
            entityManager.persist(post);
            entityManager.getTransaction().commit();
            return true;

        } catch (Exception e){
            e.printStackTrace();
        }
        return false;

    }

    public static Post getPostById(int post_id) {
        try{
            TypedQuery<Post> query = entityManager.createQuery("select post from Post post WHERE post.idPost=:post_id", Post.class);
            return query.setParameter("post_id", post_id).getResultList().stream().findFirst().get();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
