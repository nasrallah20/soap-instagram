package dao;

import model.Post;
import model.User;

import javax.persistence.TypedQuery;
import java.util.List;

public class UserDao extends DAO {

    public static User checkLogin(String username ,String password){
        User user = getUserById(username);
        if (user != null && password.equals(user.getPassword())) {
            return user;
        }
        return null;
    }

    public static User getUserById(String username) {
        User user = new User();
        try{
            TypedQuery<User> query = entityManager.createQuery("select user from User user WHERE user.username=:username", User.class);
            query.setParameter("username", username);
            user = query.getSingleResult();
            return user;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static List<Post> getPostsByUserId(String username){
        List<Post> posts = null;
        User user = getUserById(username);
        try{
            TypedQuery<Post> query = entityManager.createQuery("select post from Post post WHERE post.user=:user", Post.class);
            query.setParameter("user", user);
            posts = query.getResultList();

            return posts;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static List<User> getAllUsers(){
        List<User> users = null;
        try {

            TypedQuery<User> query = entityManager.createQuery("SELECT user.username FROM User user", User.class);
            users = query.getResultList();
            return users;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
