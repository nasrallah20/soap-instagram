package model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class User {

	@Id
	private String username;

	private String password;

	public  User(){
		//posts = new ArrayList<Post>();
	}
	public User(String username, String password){
		this.username = username;
		this.password = password;

	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String usename) {
		this.username = usename;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
