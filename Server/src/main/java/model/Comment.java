package model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class Comment   {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idComment;

	private String comment;

	@ManyToOne(fetch= FetchType.LAZY)
	@JoinColumn(name = "userId")
	private User user;


	public Comment(){}
	public Comment(String comment){
		this.comment = comment;
	}


	public int getIdComment() {
		return idComment;
	}
	public void setIdComment(int idComment) {
		this.idComment = idComment;
	}

	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}




}
