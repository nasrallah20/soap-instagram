package model;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@XmlRootElement
@Entity
public class Post   {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPost;

	private String description;

	@ManyToOne
	@JoinColumn(name = "userId")
	private User user;

	@Lob
	private byte[] fileimage;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "postId")
	List<Comment> comments = new ArrayList<>();

	@Transient
	String base64;


	public  Post()
	{
	}
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public int getIdPost() {
		return idPost;
	}

	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getFileimage() {
		return fileimage;
	}

	public void setFileimage(byte[] fileimage) {
		this.fileimage = fileimage;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void addComment(Comment comment) {
		comments.add(comment);
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getBase64() {
		return base64;
	}
}
