<%--
  Created by IntelliJ IDEA.
  User: yangyang
  Date: 2020/5/25
  Time: 1:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<style>
  .card {
    margin: 0 auto; /* Added */
    float: none; /* Added */
    margin-bottom: 10px; /* Added */
  }
</style>
<html>
  <head>
    <title>Posts</title>
  </head>

  <body  style="margin-top: 10%">

  <div class="navbar navbar-expand-md fixed-top"  style="background-color: darkmagenta">

    <div class="collapse navbar-collapse">
      <div class="navbar-header">
        <c:if test="${sessionScope.userConnected!=null}">
          <a class="navbar-brand" style="color: white"  href="#">Bienvenue : ${sessionScope.userConnected.username}</a>
        </c:if>
      </div>
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" style="color: white" href="${pageContext.request.contextPath}/posts">Posts</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" style="color: white" href="${pageContext.request.contextPath}/addPost">Add your Post</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" style="color: white" href="${pageContext.request.contextPath}/postsByUser">Posts By User</a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a style="color: white" href="${pageContext.request.contextPath}/logout"> Sign Out</a></li>
      </ul>
    </div>
  </div>

  <c:forEach items="${allposts}" var="post">
    <div class="card" style="width: 35rem;"  >
      <div  class="container" style="padding-left: 10px">
          <div class="row">
            <div class="col-7" style="color: darkmagenta">  ${post.user.username}</div>
          </div>
      </div>

      <img src="data:image/jpg;base64,${post.base64}" class="card-img-top"/>
      <div class="card-body">
        <p class="card-text">${post.description}</p>
      </div>

      <c:forEach items='${post.comments}' var="comment">
        <div  class="container" style="padding-left: 25px">
          <div class="row">
            <div class="col-3"> <strong>${comment.user.username} </strong> </div>
            <div class="col-9">  ${comment.comment}</div>
          </div>
        </div>
      </c:forEach>



      <a href='${pageContext.request.contextPath}/addComment?post_id=${post.idPost}'>
        <button style="background-color: darkmagenta;" formaction="" class="btn btn-primary" >add comment</button>
      </a>
    </div>
    </c:forEach>



  </body>
</html>
