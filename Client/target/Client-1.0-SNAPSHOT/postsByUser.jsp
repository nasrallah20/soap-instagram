<%--
  Created by IntelliJ IDEA.
  User: nournasrallah
  Date: 02/06/2020
  Time: 02:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@600&display=swap" rel="stylesheet">"

<html>
<head>
    <title>Posts By User</title>
</head>
<style>

    body {
        font-family: 'Raleway', sans-serif;
    }
</style>

<body>

<form action="list" method="post">
    Select a Category:&nbsp;
    <select name="category">
        <c:forEach items="${listCategory}" var="category">
            <option>${category.username}</option>
        </c:forEach>
    </select>
    <br/><br/>
    <input type="submit" value="Submit" />
</form>

 <c:forEach items="${allposts}" var="post">
    <div class="card" style="width: 35rem;"  >
    <div  class="container" style="padding-left: 10px">
    <div class="row">
    <div class="col-7" style="color: darkmagenta">  ${post.user.username}</div>
    </div>
    </div>

    <img src="data:image/jpg;base64,${post.base64}" class="card-img-top"/>
    <div class="card-body">
    <p class="card-text">${post.description}</p>
    </div>

    <c:forEach items='${post.comments}' var="comment">
        <div  class="container" style="padding-left: 25px">
            <div class="row">
                <div class="col-3"> <strong>${comment.user.username} </strong> </div>
                <div class="col-9">  ${comment.comment}</div>
            </div>
        </div
    </c:forEach>
 </c:forEach>



</body>
</html>
