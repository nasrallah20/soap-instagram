package servlet;

import client.Comment.CommentClient;
import client.Comment.CommentServiceStub;
import client.User.UserServiceStub;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(name = "CommentServlet", urlPatterns = "/addComment")

public class AddCommentServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        UserServiceStub.User userConnected = (UserServiceStub.User) session.getAttribute("userConnected");

        RequestDispatcher disp;
        String post_id = (String) session.getAttribute("post_id");

        CommentServiceStub.Post post = CommentClient.getPostById(Integer.parseInt(post_id));

        String comment = request.getParameter("comment");

        CommentServiceStub.Comment newComment = new CommentServiceStub.Comment();

        newComment.setUser(CommentClient.getUserComment(userConnected));
        newComment.setComment(comment);

        boolean res = CommentClient.addComment(newComment, Integer.parseInt(post_id));

        if (res == false){
            request.setAttribute("commentAlert", "Add comment fail");
            disp = request.getRequestDispatcher("/addComment.jsp");
            disp.forward(request, response);
        }else{
            response.sendRedirect("posts");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher disp;
        String post_id = request.getParameter("post_id");
        session.setAttribute("post_id", post_id);
        disp = request.getRequestDispatcher("/addComment.jsp");
        disp.forward(request, response);



    }
}
