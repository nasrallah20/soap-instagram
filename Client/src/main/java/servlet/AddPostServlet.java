package servlet;

import client.Post.PostClient;
import client.Post.PostServiceStub;
import client.User.UserServiceStub;
import com.sun.istack.ByteArrayDataSource;
import org.apache.commons.io.IOUtils;

import javax.activation.DataHandler;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;


@WebServlet(name = "AddPost", urlPatterns = "/addPost")
@MultipartConfig(location="/tmp", fileSizeThreshold=1024*1024,
        maxFileSize=1024*1024*5, maxRequestSize=1024*1024*5*5)

public class AddPostServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Récupération du user et de la descritpion
        HttpSession session = request.getSession();
        UserServiceStub.User userConnected = (UserServiceStub.User) session.getAttribute("userConnected");
        String description = request.getParameter("description");

        //récupération de la date
        java.util.Date currentDate = Calendar.getInstance().getTime();
        java.sql.Date date = new java.sql.Date(currentDate.getTime());

        Part image = request.getPart("image");
        //Part image = request.
        InputStream fileContent = image.getInputStream();

        byte[] bytes = IOUtils.toByteArray(fileContent);

        ByteArrayDataSource imageSource = new ByteArrayDataSource(bytes, "image/jpeg");

        PostServiceStub.Post post = new PostServiceStub.Post();

        post.setUser(PostClient.getUserPost(userConnected));
        post.setFileimage(new DataHandler(imageSource));
        post.setDescription(description);

        PostClient.addPost(post);

        response.sendRedirect("posts");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher disp;
        disp = request.getRequestDispatcher("/addPost.jsp");
        disp.forward(request, response);
    }
}
