package servlet;

import client.User.UserClient;
import client.User.UserServiceStub;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "SigninServlet" ,urlPatterns = "/connexion")
public class ConnexionServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        RequestDispatcher disp;
        String username = request.getParameter("username");
        String pass = request.getParameter("password");

        UserServiceStub.User user = UserClient.login(username, pass);

        if(user != null) {
            request.getSession().setAttribute("userConnected", user);
            response.sendRedirect("posts");
        }
        else {
            request.setAttribute("msg", "Username or password not correct");
            disp = request.getRequestDispatcher("/login.jsp");
            disp.forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher disp;
        disp = req.getRequestDispatcher("/login.jsp");
        disp.forward(req, resp);
    }
}
