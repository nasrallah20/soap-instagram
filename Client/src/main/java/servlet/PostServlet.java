package servlet;



import client.Post.PostClient;
import client.Post.PostServiceStub;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "PostServlet", urlPatterns = "/posts")
public class PostServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<PostServiceStub.Post> posts = PostClient.getPosts();

        request.setAttribute("allposts", posts);

        request.getRequestDispatcher("/posts.jsp").forward(request, response);



    }
}
