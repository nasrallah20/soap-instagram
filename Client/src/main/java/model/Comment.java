package model;

import java.util.Date;

public class Comment {

	private int idComment;
	private Post post;
	private String comment;
	private User user;
	private Date commentDate;

	public Comment(){}

	public int getIdComment() {
		return idComment;
	}
	public void setIdComment(int idComment) {
		this.idComment = idComment;
	}

	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Post getPostImage() {
		return post;
	}

	public void setPostImage(Post post) {
		this.post = post;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public Comment(String comment){
		this.comment = comment;
	}
}
