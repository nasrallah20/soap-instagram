package client.User;

import java.rmi.RemoteException;

public class UserClient {


    public static UserServiceStub.User login(String username, String password) throws RemoteException {

        UserServiceStub client = new UserServiceStub();
        UserServiceStub.Login request = new UserServiceStub.Login();
        request.setUsername(username);
        request.setPassword(password);

        UserServiceStub.LoginResponse response = client.login(request);

        return response.get_return();

    }
}
