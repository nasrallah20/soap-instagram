/**
 * CommentServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.9  Built on : Nov 16, 2018 (12:05:37 GMT)
 */
package client.Comment;


/**
 *  CommentServiceCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class CommentServiceCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public CommentServiceCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public CommentServiceCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for postById method
     * override this method for handling normal response from postById operation
     */
    public void receiveResultpostById(
        client.Comment.CommentServiceStub.PostByIdResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from postById operation
     */
    public void receiveErrorpostById(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for addComment method
     * override this method for handling normal response from addComment operation
     */
    public void receiveResultaddComment(
        client.Comment.CommentServiceStub.AddCommentResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from addComment operation
     */
    public void receiveErroraddComment(java.lang.Exception e) {
    }
}
