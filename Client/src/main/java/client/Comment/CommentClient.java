package client.Comment;

import client.User.UserServiceStub;

import java.rmi.RemoteException;


public class CommentClient {

    public static boolean addComment(CommentServiceStub.Comment comment, int post_id) throws RemoteException {

        CommentServiceStub client = new CommentServiceStub();

        CommentServiceStub.AddComment req = new CommentServiceStub.AddComment();
        req.setComment(comment);
        req.setPost(getPostById(post_id));

        CommentServiceStub.AddCommentResponse resp = client.addComment(req);

        return resp.get_return();

    }

    public static CommentServiceStub.User getUserComment(UserServiceStub.User user){
        CommentServiceStub.User userComment = new CommentServiceStub.User();
        userComment.setPassword(user.getPassword());
        userComment.setUsername(user.getUsername());
        return userComment;
    }

    public static CommentServiceStub.Post getPostById(int idPost) throws RemoteException {

        CommentServiceStub client = new CommentServiceStub();

        CommentServiceStub.PostById req = new CommentServiceStub.PostById();
        req.setPostId(idPost);

        CommentServiceStub.PostByIdResponse resp = client.postById(req);

        return resp.get_return();
    }

}
