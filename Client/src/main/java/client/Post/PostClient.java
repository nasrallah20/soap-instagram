package client.Post;

import client.User.UserServiceStub;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;


public class PostClient {

   public static List<PostServiceStub.Post> getPosts() throws RemoteException {

        PostServiceStub client = new PostServiceStub();

        PostServiceStub.AllPosts req = new PostServiceStub.AllPosts();

        PostServiceStub.AllPostsResponse resp = client.allPosts(req);


        PostServiceStub.Post[] postsArr = resp.get_return();

        List<PostServiceStub.Post> postList = Arrays.asList(postsArr);

        return postList;
    }


    public static boolean addPost(PostServiceStub.Post post) throws RemoteException {

        PostServiceStub client = new PostServiceStub();

        PostServiceStub.AddPost request = new PostServiceStub.AddPost();

        request.setPost(post);

        PostServiceStub.AddPostResponse response = client.addPost(request);

        return response.get_return();
    }


    public static PostServiceStub.User getUserPost(UserServiceStub.User user){
        PostServiceStub.User userPost = new PostServiceStub.User();
        userPost.setPassword(user.getPassword());
        userPost.setUsername(user.getUsername());
        return userPost;
    }


    public static PostServiceStub.Post getPostById(int id) throws RemoteException {

        PostServiceStub client = new PostServiceStub();

        PostServiceStub.GetPostById request = new PostServiceStub.GetPostById();

        request.setPostId(id);

        PostServiceStub.GetPostByIdResponse response = client.getPostById(request);

        return response.get_return();
    }

    public static UserServiceStub.User getUser(PostServiceStub.User userPost){
        UserServiceStub.User user = new UserServiceStub.User();
        user.setPassword(userPost.getPassword());
        user.setUsername(userPost.getUsername());
        return user;
    }


}
