/**
 * PostServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.9  Built on : Nov 16, 2018 (12:05:37 GMT)
 */
package client.Post;


/**
 *  PostServiceCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class PostServiceCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public PostServiceCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public PostServiceCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for allPosts method
     * override this method for handling normal response from allPosts operation
     */
    public void receiveResultallPosts(
        client.Post.PostServiceStub.AllPostsResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from allPosts operation
     */
    public void receiveErrorallPosts(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for addPost method
     * override this method for handling normal response from addPost operation
     */
    public void receiveResultaddPost(
        client.Post.PostServiceStub.AddPostResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from addPost operation
     */
    public void receiveErroraddPost(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for getPostById method
     * override this method for handling normal response from getPostById operation
     */
    public void receiveResultgetPostById(
        client.Post.PostServiceStub.GetPostByIdResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from getPostById operation
     */
    public void receiveErrorgetPostById(java.lang.Exception e) {
    }
}
